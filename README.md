# Exercise08

Simple app that shows a custom component which renders one of three shapes on the display. The shapes are a square,
a circle and a rectangle. When the user taps on the shape, the next one in the sequence will replace the current shape.

On top of each shape, a text string indicates which shape is being rendered.

