package com.unox.exercise08

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.TypedValue
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import kotlin.math.min

class MutableShape @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0, defStyleRes: Int = 0) : View(context, attrs, defStyleAttr, defStyleRes) {
    private val shapePaint: Paint
    private val textPaint: Paint

    private var triangleColor: Int
    private var circleColor: Int
    private var rectColor: Int

    // Holds information about the currently shown shape
    // 0 = circle, 1 = rect, 2 = triangle
    private var currentShape = 2

    // Radius of the circle
    private var radius = 0f

    // X and Y coordinates of the center of the circle
    private var cX = 0f
    private var cY = 0f

    private var textBoundRect = Rect()
    private val textBottomPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics)

    private lateinit var mGestureDetector: GestureDetector


    private val mGestureListener = object: GestureDetector.SimpleOnGestureListener() {
        override fun onDown(e: MotionEvent?): Boolean {
            return true
        }

        override fun onSingleTapUp(e: MotionEvent?): Boolean {
            currentShape = if (currentShape < 2) currentShape + 1 else 0
            return true
        }
    }


    init {
        shapePaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            style = Paint.Style.FILL
        }

        textPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = Color.BLACK
            style = Paint.Style.FILL

        }

        context.obtainStyledAttributes(attrs, R.styleable.MutableShape,0,0).apply {
            try {
                triangleColor = getColor(R.styleable.MutableShape_triangleColor, Color.RED)
                circleColor = getColor(R.styleable.MutableShape_circleColor, Color.CYAN)
                rectColor = getColor(R.styleable.MutableShape_rectColor, Color.GREEN)

                textPaint.textSize = getDimension(R.styleable.MutableShape_textSize, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 20f, resources.displayMetrics))
            } finally {
                recycle()
            }
        }

        if (!isInEditMode) {
            mGestureDetector = GestureDetector(context, mGestureListener)
        }

        calculateTextBoundingBox()
    }



    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val minW: Int = paddingLeft + paddingRight + suggestedMinimumWidth
        val w = resolveSize(minW, widthMeasureSpec)

        val minH = paddingBottom + paddingTop + View.MeasureSpec.getSize(heightMeasureSpec)
        val h = resolveSize(minH, heightMeasureSpec)

        val dim = min(w, h)

        setMeasuredDimension(dim, dim)
    }


    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)

        // Once the final size of the view has been calculated, we can determine the radius and X,Y coordinates
        // of the center of the circle. Drawing will start from that point.
        calculateTextBoundingBox()
        radius = (min(w, h).toFloat() - textBoundRect.height().toFloat() -  textBottomPadding) / 2
        cX = w.toFloat()/2
        cY = (h + textBoundRect.height() + textBottomPadding)/2
    }



    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (mGestureDetector.onTouchEvent(event)) {
            calculateTextBoundingBox()
            invalidate()
            return true
        }
        return super.onTouchEvent(event)
    }


    private fun calculateTextBoundingBox() {
        when(currentShape) {
            0 -> textPaint.getTextBounds("CIRCLE", 0, "CIRCLE".length, textBoundRect)
            1 -> textPaint.getTextBounds("RECT", 0, "RECT".length, textBoundRect)
            2 -> textPaint.getTextBounds("TRIANGLE", 0, "TRIANGLE".length, textBoundRect)
        }
    }


    /**********************************************************
     *                       Drawing methods                  *
     *********************************************************/

    override fun onDraw(canvas: Canvas?) {
        canvas?.apply {
            when (currentShape) {
                0 -> paintCircle(canvas)
                1 -> paintRect(canvas)
                2 -> paintTriangle(canvas)
            }


        }
    }



    private fun paintCircle(canvas: Canvas) {
        shapePaint.color = circleColor
        canvas.apply {
            drawText("CIRCLE", cX - textBoundRect.width().toFloat() / 2, cY - radius - textBottomPadding, textPaint)
            drawCircle(cX, cY, radius, shapePaint)
        }
    }


    private fun paintRect(canvas: Canvas) {
        shapePaint.color = rectColor
        canvas.apply {
            drawText("RECT", cX - textBoundRect.width().toFloat() / 2, cY - radius - textBottomPadding, textPaint)
            drawRect(cX - radius,cY - radius, cX + radius, cY + radius, shapePaint)
        }
    }


    private fun paintTriangle(canvas: Canvas) {
        shapePaint.color = triangleColor
        canvas.apply {
            drawText("TRIANGLE", cX - textBoundRect.width().toFloat() / 2, cY - radius - textBottomPadding, textPaint)
            val topVertexX = cX
            val topVertexY = cY - radius

            val mPath = Path()
            mPath.moveTo(topVertexX, topVertexY)
            mPath.lineTo(cX - radius, cY + radius)
            mPath.rLineTo(2*radius,0f)
            mPath.lineTo(topVertexX, topVertexY)

            canvas.drawPath(mPath, shapePaint)
        }
    }
}